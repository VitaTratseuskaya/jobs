package by.itstep.jobs.repository;

import by.itstep.jobs.entity.UserEntity;
import by.itstep.jobs.repository.interfaces.UserRepository;
import by.itstep.jobs.utils.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class UserHibernateRepository implements UserRepository {

    @Override
    public UserEntity findById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        UserEntity foundUser = em.find(UserEntity.class, id);

        Hibernate.initialize(foundUser.getInterviews());

        em.getTransaction().commit();
        em.close();
        return foundUser;
    }

    @Override
    public List<UserEntity> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        List<UserEntity> allUser =
                em.createNativeQuery("SELECT * FROM user", UserEntity.class).getResultList();

        em.getTransaction().commit();
        em.close();
        return allUser;
    }

    @Override
    public UserEntity create(UserEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(entity);

        em.getTransaction().commit();
        em.close();

        return entity;
    }

    @Override
    public UserEntity upDate(UserEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(entity);

        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Override
    public void delete(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        UserEntity deleteUser = em.find(UserEntity.class, id);
        em.remove(deleteUser);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM user").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public UserEntity findByEmail(String userEmail) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        String sql = String.format("SELECT * FROM user WHERE email = '%s';", userEmail);
        UserEntity foundUser = (UserEntity) em.createNativeQuery(sql, UserEntity.class).getSingleResult();

        em.getTransaction().commit();
        em.close();

        return foundUser;
    }


}
