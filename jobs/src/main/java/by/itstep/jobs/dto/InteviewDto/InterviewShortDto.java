package by.itstep.jobs.dto.InteviewDto;


import lombok.Data;

import java.sql.Date;

@Data
public class InterviewShortDto {

    private Integer interviewId;

    private String status;
    private Date interviewDate;
    private Integer userId;
    private Integer vacancyId;

}
