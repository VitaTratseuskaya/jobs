package by.itstep.jobs.service;

import by.itstep.jobs.dto.UserDto.UserCreateDto;
import by.itstep.jobs.dto.UserDto.UserFullDto;
import by.itstep.jobs.dto.VacancyDto.VacancyCreateDto;
import by.itstep.jobs.dto.VacancyDto.VacancyFullDto;
import by.itstep.jobs.dto.VacancyDto.VacancyShortDto;
import by.itstep.jobs.dto.VacancyDto.VacancyUpdateDto;
import by.itstep.jobs.entity.UserEntity;
import by.itstep.jobs.entity.VacancyEntity;
import by.itstep.jobs.repository.UserHibernateRepository;
import by.itstep.jobs.repository.VacancyHibernateRepository;
import by.itstep.jobs.repository.interfaces.VacancyRepository;
import by.itstep.jobs.util.GenerateUtil;
import by.itstep.jobs.utils.DataBaseCleaner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class VacancyServiceTest {
    //given
    //when
    //then

    @Autowired
    private VacancyService vacancyService;
    @Autowired
    private VacancyRepository vacancyRepository;
    @Autowired
    private DataBaseCleaner cleaner;

    @BeforeEach
    public void setUp() {
        cleaner.clean();
    }

    @AfterEach
    private void shutDown() {
        cleaner.clean();
    }

    @Test
    public void create_happyPath() {
        //givenV
        VacancyCreateDto vacancy = GenerateUtil.prepareVacancyCreateDto();
        //when
        VacancyShortDto createdVacancy = vacancyService.create(vacancy);
        //then
        Assertions.assertNotNull(createdVacancy.getVacancyId());
    }

    @Test
    public void findById_happyPath() {
        //given
        VacancyEntity vacancy = addVacancyToDataBase();
        //when
        VacancyShortDto found = vacancyService.findById(vacancy.getVacancyId());
        //then
        Assertions.assertEquals(vacancy.getVacancyId(), found.getVacancyId());
    }

    @Test
    public void findById_IdNull() {
        //gvien
        boolean vacancyNull = false;
        VacancyEntity vacancy = GenerateUtil.prepareVacancy();
        vacancy.setVacancyId(0);

        //when
        try {
            vacancyService.findById(vacancy.getVacancyId());
        } catch (RuntimeException ex) {
            vacancyNull = true;
        }

        //then
        Assertions.assertTrue(vacancyNull);
    }

    @Test
    public void findAll_isEmpty() {
        //given

        //when
        List<VacancyShortDto> allVacancy = vacancyService.findAll();
        //then
        Assertions.assertTrue(allVacancy.isEmpty());
    }

    @Test
    public void findAll_happyPath() {
        //given
        addVacancyToDataBase();
        //when
        List<VacancyShortDto> allVacancy = vacancyService.findAll();
        //then
        Assertions.assertEquals(1, allVacancy.size());
    }

    @Test
    public void upDate_withoutId() {
        //given
        boolean upDateNull = false;
        VacancyUpdateDto vacancy = GenerateUtil.prepareVacancyUpdateDto();
        vacancy.setVacancyId(0);

        //when
        try {
            vacancyService.upDate(vacancy);
        } catch (RuntimeException ex) {
            upDateNull = true;
        }

        //then
        Assertions.assertTrue(upDateNull);
    }

    @Test
    public void upDate_happyPath() {
        //given
        VacancyShortDto existingVacancy = addVacancyDtoToDataBase();
        VacancyUpdateDto updateRequest = new VacancyUpdateDto();
        updateRequest.setVacancyId(existingVacancy.getVacancyId());
        updateRequest.setName("update");
        updateRequest.setSalary(10);

        //when
        VacancyShortDto upDatedUser = vacancyService.upDate(updateRequest);

        //then
        Assertions.assertEquals(existingVacancy.getVacancyId(), upDatedUser.getVacancyId());
        VacancyEntity vacancyAfterUpdate = vacancyRepository.findById(existingVacancy.getVacancyId());

        Assertions.assertEquals(vacancyAfterUpdate.getName(), upDatedUser.getName());
        Assertions.assertEquals(vacancyAfterUpdate.getSalary(), upDatedUser.getSalary());
    }

    @Test
    public void delete_happyPath() {
        //given
        VacancyEntity existVacancy = addVacancyToDataBase();
        //when
        vacancyService.delete(existVacancy.getVacancyId());
        //then
        VacancyEntity foundVacancy = vacancyRepository.findById(existVacancy.getVacancyId());
        Assertions.assertNull(foundVacancy);
    }

    @Test
    public void delete_withoutId() {
        //given
        boolean deleteNull = false;

        VacancyEntity existVacancy = addVacancyToDataBase();
        existVacancy.setVacancyId(0);

        //when
        try {
            vacancyService.delete(existVacancy.getVacancyId());
        } catch (RuntimeException ex) {
            deleteNull = true;
        }

        //then
        Assertions.assertTrue(deleteNull);
    }

    private VacancyEntity addVacancyToDataBase() {
        VacancyEntity vacancyToAdd = GenerateUtil.prepareVacancy();
        return vacancyRepository.create(vacancyToAdd);
    }

    private VacancyShortDto addVacancyDtoToDataBase() {
        VacancyCreateDto createDto = GenerateUtil.prepareVacancyCreateDto();
        return vacancyService.create(createDto);
    }


}
