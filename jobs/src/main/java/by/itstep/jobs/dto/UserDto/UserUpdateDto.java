package by.itstep.jobs.dto.UserDto;


import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserUpdateDto {

    @NotNull
    private Integer userId;

    @NotBlank(message = "First name can not by blank")
    private String firstName;

    @NotBlank(message = "Last name can not by blank")
    private String lastName;

    @NotBlank(message = "Phone can not by blank")
    @Size(min = 9, max = 9, message = "The number must have 9 digits")
    private String phone;

    @NotBlank(message = "Position can not by blank")
    private String position;

    private Integer yearOfExperience;

}
