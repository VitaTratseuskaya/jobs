package by.itstep.jobs.repository;

import by.itstep.jobs.entity.InterviewEntity;
import by.itstep.jobs.repository.interfaces.InterviewRepository;
import by.itstep.jobs.utils.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class InterviewHibernateRepository implements InterviewRepository {

    @Override
    public InterviewEntity findById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        InterviewEntity foundInterview = em.find(InterviewEntity.class, id);

        em.getTransaction().commit();
        em.close();
        return foundInterview;
    }

    @Override
    public List<InterviewEntity> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        List<InterviewEntity> allInterview =
                em.createNativeQuery("SELECT * FROM interview", InterviewEntity.class).getResultList();

        em.getTransaction().commit();
        em.close();
        return allInterview;
    }

    @Override
    public InterviewEntity create(InterviewEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.persist(entity); //сохранить

        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Override
    public InterviewEntity upDate(InterviewEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(entity);

        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Override
    public void delete(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        InterviewEntity deleteInterview = em.find(InterviewEntity.class, id);
        em.remove(deleteInterview);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM interview").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
