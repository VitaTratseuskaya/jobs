package by.itstep.jobs.dto.InteviewDto;

import by.itstep.jobs.entity.UserEntity;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
public class InterviewCreateDto {

    @NotBlank(message = "Name can not by blank")
    private String status;

    @NotNull
    private Integer userId;

    @NotNull
    private Integer vacancyId;

}
