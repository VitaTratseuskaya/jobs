package by.itstep.jobs.mapper;

import by.itstep.jobs.dto.InteviewDto.InterviewShortDto;
import by.itstep.jobs.dto.UserDto.UserCreateDto;
import by.itstep.jobs.dto.UserDto.UserFullDto;
import by.itstep.jobs.dto.UserDto.UserShortDto;
import by.itstep.jobs.dto.UserDto.UserUpdateDto;
import by.itstep.jobs.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserMapper {

    @Autowired
    private InterviewMapper interviewMapper;

    public UserFullDto map(UserEntity entity) {
        UserFullDto dto = new UserFullDto();

        dto.setUserId(entity.getUserId());
        dto.setEmail(entity.getEmail());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());

        dto.setPhone(entity.getPhone());
        dto.setPosition(entity.getPosition());
        dto.setYearOfExperience(entity.getYearOfExperience());
        dto.setRole(entity.getRole());

        List<InterviewShortDto> interviewDtos = interviewMapper.map(entity.getInterviews());
        dto.setInterviews(interviewDtos);

        return dto;
    }

    public List<UserShortDto> map(List<UserEntity> entities) {

        List<UserShortDto> dtos = new ArrayList<>();
        for (UserEntity entity : entities) {
            UserShortDto dto = new UserShortDto();
            dto.setUserId(entity.getUserId());
            dto.setEmail(entity.getEmail());
            dto.setPhone(entity.getPhone());
            dto.setFirstName(entity.getFirstName());
            dto.setLastName(entity.getLastName());

            dtos.add(dto);
        }
        return dtos;
    }

    public UserEntity map(UserCreateDto dto) {
        UserEntity entity = new UserEntity();
        entity.setEmail(dto.getEmail());
        entity.setFirstName(dto.getFirstName());
        entity.setLastName(dto.getLastName());
        entity.setPhone(dto.getPhone());

        entity.setRole(dto.getRole());
        entity.setPassword(dto.getPassword());
        entity.setPosition(dto.getPosition());
        entity.setYearOfExperience(dto.getYearOfExperience());

        return entity;
    }

    public UserShortDto mapToShort(UserEntity entity) {
        UserShortDto dto = new UserShortDto();

        dto.setUserId(entity.getUserId());
        dto.setEmail(entity.getEmail());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        dto.setPhone(entity.getPhone());

        return dto;
    }

}
