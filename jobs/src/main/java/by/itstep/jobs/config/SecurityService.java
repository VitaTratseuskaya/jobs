package by.itstep.jobs.config;

import by.itstep.jobs.entity.UserEntity;
import by.itstep.jobs.repository.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class SecurityService {

    @Autowired
    private UserRepository userRepository;

    public UserEntity getAuthenticateUser() {
        if (getAuthenticateUser() == null) {
            return null;
        }

        String email = SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getName();

        return  userRepository.findByEmail(email);
    }
}
