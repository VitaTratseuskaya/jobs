package by.itstep.jobs.service;

import by.itstep.jobs.dto.InteviewDto.InterviewCreateDto;
import by.itstep.jobs.dto.InteviewDto.InterviewFullDto;
import by.itstep.jobs.dto.InteviewDto.InterviewShortDto;
import by.itstep.jobs.dto.InteviewDto.InterviewUpdateDto;
import by.itstep.jobs.entity.InterviewEntity;
import by.itstep.jobs.entity.UserEntity;
import by.itstep.jobs.entity.VacancyEntity;
import by.itstep.jobs.repository.InterviewHibernateRepository;
import by.itstep.jobs.repository.UserHibernateRepository;
import by.itstep.jobs.repository.VacancyHibernateRepository;
import by.itstep.jobs.repository.interfaces.InterviewRepository;
import by.itstep.jobs.repository.interfaces.UserRepository;
import by.itstep.jobs.repository.interfaces.VacancyRepository;
import by.itstep.jobs.util.GenerateUtil;
import by.itstep.jobs.utils.DataBaseCleaner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class InterviewServiceTest {

    @Autowired
    private InterviewService interviewService;

    @Autowired
    private InterviewRepository interviewRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private VacancyRepository vacancyRepository;

    @Autowired
    private DataBaseCleaner cleaner;

    @BeforeEach
    private void setUp() {
        cleaner.clean();
    }

    @AfterEach
    private void shutDown() {
        cleaner.clean();
    }


    @Test
    public void create_happyPath() {
        //given
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        VacancyEntity vacancy = GenerateUtil.prepareVacancy();
        vacancyRepository.create(vacancy);

        InterviewCreateDto interview = GenerateUtil.prepareInterviewCreateDto(user, vacancy);

        //when
        InterviewShortDto createdInterview = interviewService.create(interview);

        //then
        Assertions.assertNotNull(createdInterview.getInterviewId());
    }

    @Test
    public void findById_happyPath() {
        //given
        InterviewEntity interview = addInterviewToDataBase();
        //when
        InterviewFullDto found = interviewService.findById(interview.getInterviewId());
        //then
        Assertions.assertEquals(interview.getInterviewId(), found.getInterviewId());
    }

    @Test
    public void findById_IdNull() {
        //given
        boolean interviewNull = false;
        UserEntity user = GenerateUtil.prepareUser();
        VacancyEntity vacancy = GenerateUtil.prepareVacancy();
        InterviewEntity interview = GenerateUtil.prepareInterview(user, vacancy);
        interview.setInterviewId(0);

        //when
        try {
            interviewService.findById(interview.getInterviewId());
        } catch (RuntimeException ex) {
            interviewNull = true;
        }
        //then
        Assertions.assertTrue(interviewNull);
    }

    @Test
    public void findAll_isEmpty() {
        //given

        //when
        List<InterviewShortDto> allInterview = interviewService.findAll();
        //then
        Assertions.assertTrue(allInterview.isEmpty());
    }

    @Test
    public void findAll_happyPath() {
        //given
        addInterviewToDataBase();
        //when
        List<InterviewShortDto> allInterview = interviewService.findAll();
        //then
        Assertions.assertEquals(1, allInterview.size());
    }

    @Test
    public void upDate_withoutId() {
        //given
        boolean upDateNull = false;
        UserEntity user = GenerateUtil.prepareUser();
        VacancyEntity vacancy = GenerateUtil.prepareVacancy();

        InterviewUpdateDto interview = GenerateUtil.prepareInterviewUpdateDto(user, vacancy);
        interview.setInterviewId(0);
        //when
        try {
            interviewService.upDate(interview);
        } catch (RuntimeException ex) {
            upDateNull = true;
        }
        //then
        Assertions.assertTrue(upDateNull);
    }

    @Test
    public void upDate_happyPath() {
        //given
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);
        VacancyEntity vacancy = GenerateUtil.prepareVacancy();
        vacancyRepository.create(vacancy);

        InterviewShortDto existingInterview = addInterviewDtoToDataBase();
        InterviewUpdateDto updateRequest = new InterviewUpdateDto();
        updateRequest.setInterviewId(existingInterview.getInterviewId());

        updateRequest.setInterviewDate(existingInterview.getInterviewDate());
        updateRequest.setStatus("update");
        //when
        InterviewFullDto upDateInterview = interviewService.upDate(updateRequest);
        //then
        Assertions.assertEquals(existingInterview.getInterviewId(), upDateInterview.getInterviewId());
        InterviewEntity interviewAfterUpdate = interviewRepository.findById(existingInterview.getInterviewId());

        Assertions.assertEquals(interviewAfterUpdate.getInterviewDate(), upDateInterview.getInterviewDate());
        Assertions.assertEquals(interviewAfterUpdate.getStatus(), upDateInterview.getStatus());
    }

    @Test
    public void delete_happyPath() {
        //given
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        VacancyEntity vacancy = GenerateUtil.prepareVacancy();
        vacancyRepository.create(vacancy);

        InterviewEntity interview = addInterviewToDataBase();

        //when
        interviewService.delete(interview.getInterviewId());

        //then
        Assertions.assertNotNull(userRepository.findById(user.getUserId()));
        Assertions.assertNotNull(vacancyRepository.findById(vacancy.getVacancyId()));
        Assertions.assertNull(interviewRepository.findById(interview.getInterviewId()));
    }

    @Test
    public void delete_withoutId() {
        //given
        boolean deleteNull = false;
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        VacancyEntity vacancy = GenerateUtil.prepareVacancy();
        vacancyRepository.create(vacancy);

        InterviewEntity interview = addInterviewToDataBase();
        interview.setInterviewId(0);

        //when
        try {
            interviewService.delete(interview.getInterviewId());
        } catch (RuntimeException ex) {
            deleteNull = true;
        }

        //then
        Assertions.assertTrue(deleteNull);
    }


    private InterviewEntity addInterviewToDataBase() {
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);
        VacancyEntity vacancy = GenerateUtil.prepareVacancy();
        vacancyRepository.create(vacancy);

        InterviewEntity interviewToAdd = GenerateUtil.prepareInterview(user, vacancy);
        return interviewRepository.create(interviewToAdd);
    }

    private InterviewShortDto addInterviewDtoToDataBase() {
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        VacancyEntity vacancy = GenerateUtil.prepareVacancy();
        vacancyRepository.create(vacancy);

        InterviewCreateDto createDto = GenerateUtil.prepareInterviewCreateDto(user, vacancy);
        return interviewService.create(createDto);
    }

}
