package by.itstep.jobs.dto.VacancyDto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class VacancyUpdateDto {

    @NotNull
    private Integer vacancyId;

    @NotBlank(message = "Name can not by blank")
    private String name;

    @NotNull
    private Integer salary;

}
