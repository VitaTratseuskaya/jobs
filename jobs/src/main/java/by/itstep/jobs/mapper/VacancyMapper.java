package by.itstep.jobs.mapper;

import by.itstep.jobs.dto.InteviewDto.InterviewShortDto;
import by.itstep.jobs.dto.VacancyDto.VacancyCreateDto;
import by.itstep.jobs.dto.VacancyDto.VacancyFullDto;
import by.itstep.jobs.dto.VacancyDto.VacancyShortDto;
import by.itstep.jobs.dto.VacancyDto.VacancyUpdateDto;
import by.itstep.jobs.entity.VacancyEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class VacancyMapper {

    @Autowired
    private InterviewMapper interviewMapper;

    public VacancyFullDto map(VacancyEntity entity) {
        //1. создать пустое ДТО
        VacancyFullDto dto = new VacancyFullDto();
        //2. заполнить
        dto.setVacancyId(entity.getVacancyId());
        dto.setName(entity.getName());
        dto.setPosition(entity.getPosition());
        dto.setSalary(entity.getSalary());
        dto.setCompanyName(entity.getCompanyName());

        List<InterviewShortDto> interviewDtos = interviewMapper.map(entity.getInterviews());
        dto.setInterviews(interviewDtos);
        // 3. вернуть
        return dto;
    }

    public List<VacancyShortDto> map(List<VacancyEntity> entities) {

        List<VacancyShortDto> dtos = new ArrayList<>();
        for (VacancyEntity entity : entities) {
            VacancyShortDto dto = new VacancyShortDto();
            dto.setVacancyId(entity.getVacancyId());
            dto.setName(entity.getName());
            dto.setSalary(entity.getSalary());
            dto.setCompanyName(entity.getCompanyName());

            dtos.add(dto);
        }
        return dtos;
    }

    public VacancyEntity map(VacancyCreateDto dto) {
        VacancyEntity entity = new VacancyEntity();
        entity.setName(dto.getName());
        entity.setPosition(dto.getPosition());
        entity.setSalary(dto.getSalary());
        entity.setCompanyName(dto.getCompanyName());

        return entity;
    }

    public VacancyShortDto mapToShort(VacancyEntity entity) {
        VacancyShortDto dto = new VacancyShortDto();

        dto.setVacancyId(entity.getVacancyId());
        dto.setName(entity.getName());
        dto.setSalary(entity.getSalary());
        dto.setCompanyName(entity.getCompanyName());

        return dto;
    }

}
