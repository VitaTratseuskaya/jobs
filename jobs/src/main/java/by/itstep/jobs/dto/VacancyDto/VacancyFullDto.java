package by.itstep.jobs.dto.VacancyDto;


import by.itstep.jobs.dto.InteviewDto.InterviewShortDto;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class VacancyFullDto {

    private Integer vacancyId;

    private String name;
    private String position;
    private Integer salary;
    private String companyName;

    private List<InterviewShortDto> interviews;

}
