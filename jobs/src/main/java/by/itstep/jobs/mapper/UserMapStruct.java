package by.itstep.jobs.mapper;

import by.itstep.jobs.dto.UserDto.UserCreateDto;
import by.itstep.jobs.dto.UserDto.UserFullDto;
import by.itstep.jobs.dto.UserDto.UserShortDto;
import by.itstep.jobs.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface UserMapStruct {

    UserMapStruct MAPPER = Mappers.getMapper(UserMapStruct.class);

    UserFullDto mapToFullDto(UserEntity entity);

    List<UserShortDto> mapToListUserShortDto(List<UserEntity> entities);

    UserEntity mapToEntity(UserCreateDto dto);

    UserShortDto mapToShortDTo(UserEntity entity);
}
