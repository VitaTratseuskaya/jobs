package by.itstep.jobs.controller;

import by.itstep.jobs.dto.VacancyDto.VacancyCreateDto;
import by.itstep.jobs.dto.VacancyDto.VacancyFullDto;
import by.itstep.jobs.dto.VacancyDto.VacancyShortDto;
import by.itstep.jobs.dto.VacancyDto.VacancyUpdateDto;
import by.itstep.jobs.service.VacancyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class VacancyController {

    @Autowired
    private VacancyService vacancyService;

    @ResponseBody
    @RequestMapping(value = "/vacancy", method = RequestMethod.POST)
    public VacancyShortDto create(@Valid @RequestBody VacancyCreateDto createdRequest) {
        VacancyShortDto createdVacancy = vacancyService.create(createdRequest);
        return createdVacancy;
    }

    @ResponseBody
    @RequestMapping(value = "/vacancy/{id}", method = RequestMethod.GET)
    public VacancyShortDto findById(@PathVariable int id) {
        VacancyShortDto vacancy = vacancyService.findById(id);
        return vacancy;
    }

    @ResponseBody
    @RequestMapping(value = "/vacancy", method = RequestMethod.GET)
    public List<VacancyShortDto> findAll() {
        List<VacancyShortDto> allVacancy = vacancyService.findAll();
        return allVacancy;
    }

    @ResponseBody
    @RequestMapping(value = "/vacancy", method = RequestMethod.PUT)
    public VacancyShortDto upDate(@Valid @RequestBody VacancyUpdateDto upDateRequest) {
        VacancyShortDto upDateVacancy = vacancyService.upDate(upDateRequest);
        return upDateVacancy;
    }

    @ResponseBody
    @RequestMapping(value = "/vacancy/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int id) {
        vacancyService.delete(id);
    }

}




