package by.itstep.jobs.controller;

import by.itstep.jobs.dto.InteviewDto.InterviewCreateDto;
import by.itstep.jobs.dto.InteviewDto.InterviewFullDto;
import by.itstep.jobs.dto.InteviewDto.InterviewShortDto;
import by.itstep.jobs.dto.InteviewDto.InterviewUpdateDto;
import by.itstep.jobs.service.InterviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class InterviewController {

    @Autowired
    private InterviewService interviewService;

    @ResponseBody
    @RequestMapping(value = "/users/{userId}/vacancies/{vacanciesId}/interviews", method = RequestMethod.POST)
    public InterviewShortDto create(@RequestBody InterviewCreateDto createRequest) {
        InterviewShortDto createdInterview = interviewService.create(createRequest);
        return createdInterview;
    }

    @ResponseBody
    @RequestMapping(value = "/interviews/{id}", method = RequestMethod.GET)
    public InterviewFullDto findById(@PathVariable int id) {
        InterviewFullDto interview = interviewService.findById(id);
        return interview;
    }

    @ResponseBody
    @RequestMapping(value = "/interviews", method = RequestMethod.GET)
    public List<InterviewShortDto> findAll() {
        List<InterviewShortDto> allInterviews = interviewService.findAll();
        return allInterviews;
    }

    @ResponseBody
    @RequestMapping(value = "/users/{userId}/vacancies/{vacanciesId}/interviews", method = RequestMethod.PUT)
    public InterviewFullDto upDate(@Valid @RequestBody InterviewUpdateDto upDateRequest) {
        InterviewFullDto upDateInterview = interviewService.upDate(upDateRequest);
        return upDateInterview;
    }

    @ResponseBody
    @RequestMapping(value = "/interviews/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int id) {
        interviewService.delete(id);
    }
}

