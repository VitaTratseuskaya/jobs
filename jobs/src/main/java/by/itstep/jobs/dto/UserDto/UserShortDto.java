package by.itstep.jobs.dto.UserDto;

import lombok.Data;

@Data
public class UserShortDto {

    private Integer userId;
    private String firstName;
    private String lastName;
    private String phone;
    private String email;

}
