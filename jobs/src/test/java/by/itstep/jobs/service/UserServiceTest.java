package by.itstep.jobs.service;

import by.itstep.jobs.dto.UserDto.UserCreateDto;
import by.itstep.jobs.dto.UserDto.UserFullDto;
import by.itstep.jobs.dto.UserDto.UserShortDto;
import by.itstep.jobs.dto.UserDto.UserUpdateDto;
import by.itstep.jobs.entity.UserEntity;
import by.itstep.jobs.entity.VacancyEntity;
import by.itstep.jobs.mapper.UserMapper;
import by.itstep.jobs.repository.UserHibernateRepository;
import by.itstep.jobs.repository.interfaces.UserRepository;
import by.itstep.jobs.util.GenerateUtil;
import by.itstep.jobs.utils.DataBaseCleaner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private DataBaseCleaner cleaner;


    @BeforeEach
    public void setUp() {
        cleaner.clean();
    }

    @AfterEach
    private void shutDown() {
        cleaner.clean();
    }

    @Test
    public void create_happyPath() {
        //given
        UserCreateDto user = GenerateUtil.prepareUserCreateDto();
        //when
        UserShortDto createdUser = userService.create(user);
        //then
        Assertions.assertNotNull(createdUser.getUserId());
    }

  /*  @Test
    public void findById_happyPath() {
        //given
        UserEntity user = addUserToDataBase();
        //when
        UserShortDto found = userService.findById(user.getUserId());
        //then
        Assertions.assertEquals(user.getUserId(), found.getUserId());
    }*/

    @Test
    public void findById_IdNull() {
        //gvien
        boolean userNull = false;
        UserEntity user = GenerateUtil.prepareUser();
        user.setUserId(0);

        //when
        try {
            userService.findById(user.getUserId());
        } catch (RuntimeException ex) {
            userNull = true;
        }

        //then
        Assertions.assertTrue(userNull);
    }

    @Test
    public void findAll_isEmpty() {
        //given

        //when
        List<UserShortDto> allUser = userService.findAll();
        //then
        Assertions.assertTrue(allUser.isEmpty());
    }

    @Test
    public void findAll_happyPath() {
        //given
        addUserToDataBase();
        //when
        List<UserShortDto> allUser = userService.findAll();
        //then
        Assertions.assertEquals(1, allUser.size());
    }

    @Test
    public void upDate_withoutId() {
        //given
        boolean upDateNull = false;
        UserUpdateDto user = GenerateUtil.prepareUserUpdateDto();
        user.setUserId(0);

        //when
        try {
            userService.upDate(user);
        } catch (RuntimeException ex) {
            upDateNull = true;
        }

        //then
        Assertions.assertTrue(upDateNull);
    }

    @Test
    public void upDate_happyPath() {
        //given
        UserShortDto existingUser = addUserDtoToDataBase();
        UserUpdateDto updateRequest = new UserUpdateDto();
        updateRequest.setUserId(existingUser.getUserId());
        updateRequest.setFirstName("update");
        updateRequest.setLastName("update");
        updateRequest.setPhone("update");
        updateRequest.setPosition("update");
        updateRequest.setYearOfExperience(1);

        //when
        UserShortDto upDatedUser = userService.upDate(updateRequest);

        //then
        Assertions.assertEquals(existingUser.getUserId(), upDatedUser.getUserId());
        UserEntity userForUpdate = userRepository.findById(existingUser.getUserId());

        Assertions.assertEquals(userForUpdate.getFirstName(), upDatedUser.getFirstName());
        Assertions.assertEquals(userForUpdate.getLastName(), upDatedUser.getLastName());
        Assertions.assertEquals(userForUpdate.getPhone(), upDatedUser.getPhone());
    }

    @Test
    public void delete_happyPath() {
        //given
        UserEntity existUser = addUserToDataBase();
        //when
        userService.delete(existUser.getUserId());
        //then
        UserEntity foundUser = userRepository.findById(existUser.getUserId());
        Assertions.assertNull(foundUser);
    }

    @Test
    public void delete_withoutId() {
        //given
        boolean deleteNull = false;

        UserEntity existUser = addUserToDataBase();
        existUser.setUserId(0);

        //when
        try {
            userService.delete(existUser.getUserId());
        } catch (RuntimeException ex) {
            deleteNull = true;
        }

        //then
        Assertions.assertTrue(deleteNull);
    }


    private UserEntity addUserToDataBase() {
        UserEntity userToAdd = GenerateUtil.prepareUser();
        return userRepository.create(userToAdd);
    }

    private UserShortDto addUserDtoToDataBase() {
        UserCreateDto createDto = GenerateUtil.prepareUserCreateDto();
        return userService.create(createDto);
    }

}
