package by.itstep.jobs.repository;

import by.itstep.jobs.entity.VacancyEntity;
import by.itstep.jobs.repository.interfaces.VacancyRepository;
import by.itstep.jobs.utils.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class VacancyHibernateRepository implements VacancyRepository {
    @Override
    public VacancyEntity findById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        VacancyEntity foundVacancy = em.find(VacancyEntity.class, id);
        Hibernate.initialize(foundVacancy.getInterviews());
        em.getTransaction().commit();
        em.close();
        return foundVacancy;
    }

    @Override
    public List<VacancyEntity> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        List<VacancyEntity> allVacancy =
                em.createNativeQuery("SELECT * FROM vacancy", VacancyEntity.class).getResultList();

        em.getTransaction().commit();
        em.close();
        return allVacancy;
    }

    @Override
    public VacancyEntity create(VacancyEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.persist(entity); //сохранить

        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Override
    public VacancyEntity upDate(VacancyEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(entity);

        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Override
    public void delete(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        VacancyEntity deleteVacancy = em.find(VacancyEntity.class, id);
        em.remove(deleteVacancy);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM vacancy").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
