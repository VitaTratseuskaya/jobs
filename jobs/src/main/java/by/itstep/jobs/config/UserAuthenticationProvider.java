package by.itstep.jobs.config;

import by.itstep.jobs.entity.UserEntity;
import by.itstep.jobs.repository.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class UserAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String userEmail = authentication.getName();
        String password = authentication.getCredentials().toString();

        System.out.println("UserEmail: " + userEmail);
        System.out.println("Password: " + password);

        UserEntity foundUser = userRepository.findByEmail(userEmail);

        if(foundUser != null && foundUser.getPassword().equals(password)) {
            return new UsernamePasswordAuthenticationToken(userEmail, password, new ArrayList<>());
        } else {
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UsernamePasswordAuthenticationToken.class);
    }
}// проверь на роли. и привенти везде где это моджет понадобиться
//а, еще ипроверить надо

