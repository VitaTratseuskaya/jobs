package by.itstep.jobs.mapper;

import by.itstep.jobs.dto.InteviewDto.InterviewCreateDto;
import by.itstep.jobs.dto.InteviewDto.InterviewFullDto;
import by.itstep.jobs.dto.InteviewDto.InterviewShortDto;
import by.itstep.jobs.entity.InterviewEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface InterviewMapStruct {

    InterviewMapStruct MAPPER = Mappers.getMapper(InterviewMapStruct.class);

    InterviewFullDto mapToFullDto(InterviewEntity entity);

    List<InterviewShortDto> mapToListShortDto(List<InterviewEntity> entities);

    InterviewEntity mapToEntity(InterviewCreateDto dto);

    InterviewShortDto mapToShort(InterviewEntity entity);
}
