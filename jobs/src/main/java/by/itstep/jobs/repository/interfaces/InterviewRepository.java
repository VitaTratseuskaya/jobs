package by.itstep.jobs.repository.interfaces;

import by.itstep.jobs.entity.InterviewEntity;

import java.util.List;

public interface InterviewRepository {

    InterviewEntity findById(int id);

    List<InterviewEntity> findAll();

    InterviewEntity create(InterviewEntity entity);

    InterviewEntity upDate(InterviewEntity entity);

    void delete(int id);

    void deleteAll();
}
