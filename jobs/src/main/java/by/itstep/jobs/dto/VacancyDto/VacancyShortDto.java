package by.itstep.jobs.dto.VacancyDto;

import lombok.Data;

@Data
public class VacancyShortDto {

    private Integer vacancyId;

    private String name;
    private Integer salary;
    private String companyName;

}
