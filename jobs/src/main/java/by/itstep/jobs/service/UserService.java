package by.itstep.jobs.service;

import by.itstep.jobs.dto.UserDto.UserCreateDto;
import by.itstep.jobs.dto.UserDto.UserFullDto;
import by.itstep.jobs.dto.UserDto.UserShortDto;
import by.itstep.jobs.dto.UserDto.UserUpdateDto;
import by.itstep.jobs.entity.UserEntity;
import by.itstep.jobs.mapper.UserMapStruct;
import by.itstep.jobs.repository.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;


    public UserShortDto create(UserCreateDto createdRequest) {
        UserEntity user = UserMapStruct.MAPPER.mapToEntity(createdRequest);

        if (user.getUserId() != null) {
            throw new RuntimeException("UserService -> Can't create entity which already has id  ");
        }

        UserEntity createdUser = userRepository.create(user);
        System.out.println("UserService -> Created user: " + createdRequest);

    return UserMapStruct.MAPPER.mapToShortDTo(createdUser);
    }

  /*  public UserShortDto findById(int id) {
        UserEntity user = userRepository.findById(id);
        if (user == null) {
            throw new RuntimeException("User not found by id " + id);
        }
        System.out.println("UserService -> Found user: " + user);

        return userMapper.mapToShort(user);
    }*/

    public UserFullDto findById(int id) {
        UserEntity user = userRepository.findById(id);
        if (user == null) {
            throw new RuntimeException("User not found by id " + id);
        }
        System.out.println("UserService -> Found user: " + user);

        return UserMapStruct.MAPPER.mapToFullDto(user);
    }

    public List<UserShortDto> findAll() {
        List<UserEntity> allUser = userRepository.findAll();
        System.out.println("UserService -> Found all user: " + allUser);

        if (allUser.isEmpty()) {
            System.out.println("List is empty");
        }

        return UserMapStruct.MAPPER.mapToListUserShortDto(allUser);
    }

    public UserShortDto upDate(UserUpdateDto updateRequest) {
        UserEntity userForUpdate = userRepository.findById(updateRequest.getUserId());

        if(userForUpdate == null) {
            throw new RuntimeException("User is not found by id " + updateRequest.getUserId());
        }
        if (updateRequest.getUserId() == null) {
            throw new RuntimeException("UserService -> " +
                    "Can't update entity without id " + updateRequest.getUserId());
        }
        if (userRepository.findById(updateRequest.getUserId()) == null) {
            throw new RuntimeException("UserService -> User not found by id: " + updateRequest.getUserId());
        }

        userForUpdate.setUserId(updateRequest.getUserId());
        userForUpdate.setFirstName(updateRequest.getFirstName());
        userForUpdate.setLastName(updateRequest.getLastName());
        userForUpdate.setPhone(updateRequest.getPhone());
        userForUpdate.setPosition(updateRequest.getPosition());
        userForUpdate.setYearOfExperience(updateRequest.getYearOfExperience());

        UserEntity upDateUser = userRepository.upDate(userForUpdate);
        System.out.println("UserService -> Found user: " + upDateUser);

        return UserMapStruct.MAPPER.mapToShortDTo(upDateUser);
    }

    public void delete(int id) {
        UserEntity userForDelete = userRepository.findById(id);
        if (userForDelete == null) {
            throw new RuntimeException("UserService -> User not found " + userForDelete.getUserId());
        }
        userRepository.delete(id);
    }


}
