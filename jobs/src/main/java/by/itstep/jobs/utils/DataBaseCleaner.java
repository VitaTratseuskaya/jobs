package by.itstep.jobs.utils;

import by.itstep.jobs.repository.*;
import by.itstep.jobs.repository.interfaces.InterviewRepository;
import by.itstep.jobs.repository.interfaces.UserRepository;
import by.itstep.jobs.repository.interfaces.VacancyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataBaseCleaner {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private VacancyRepository vacancyRepository;
    @Autowired
    private InterviewRepository interviewRepository;


    public void clean() {
        userRepository = new UserHibernateRepository();
        vacancyRepository = new VacancyHibernateRepository();
        interviewRepository = new InterviewHibernateRepository();

        interviewRepository.deleteAll();
        userRepository.deleteAll();
        vacancyRepository.deleteAll();
    }

}
