package by.itstep.jobs.repository.interfaces;

import by.itstep.jobs.entity.VacancyEntity;

import java.util.List;

public interface VacancyRepository {

    VacancyEntity findById(int id);

    List< VacancyEntity> findAll();

    VacancyEntity create(VacancyEntity entity);

    VacancyEntity upDate(VacancyEntity entity);

    void delete(int id);

    void deleteAll();
}
