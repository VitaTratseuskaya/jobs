package by.itstep.jobs.repository;

import by.itstep.jobs.entity.VacancyEntity;
import by.itstep.jobs.repository.interfaces.VacancyRepository;
import by.itstep.jobs.util.GenerateUtil;
import by.itstep.jobs.utils.DataBaseCleaner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class VacancyRepositoryTest {

    @Autowired
    private VacancyRepository vacancyRepository;
    @Autowired
    private DataBaseCleaner cleaner;

    @BeforeEach
    private void setUp() {
        cleaner.clean();
    }

    @AfterEach
    private void shutDown() {
        cleaner.clean();
    }

    @Test
    public void create_happyPath(){
        //given
        VacancyEntity vacancy = GenerateUtil.prepareVacancy();

        //when
        VacancyEntity createdVacancy = vacancyRepository.create(vacancy);

        //then
        Assertions.assertNotNull(createdVacancy.getVacancyId());
        VacancyEntity foundVacancy = vacancyRepository.findById(createdVacancy.getVacancyId());

        Assertions.assertEquals(vacancy.getName(), foundVacancy.getName());
        Assertions.assertEquals(vacancy.getPosition(), foundVacancy.getPosition());
        Assertions.assertEquals(vacancy.getSalary(), foundVacancy.getSalary());
        Assertions.assertEquals(vacancy.getCompanyName(), foundVacancy.getCompanyName());
    }

    @Test
    public void findById_happyPath() {
        //given
        VacancyEntity vacancy = addVacancyToDataBase();
        //when
        VacancyEntity found = vacancyRepository.findById(vacancy.getVacancyId());
        //then
        Assertions.assertEquals(vacancy.getVacancyId(), found.getVacancyId());
    }

    @Test
    public void findAll_whenNoOneFound(){
        //given

        //when
        List<VacancyEntity> foundVacancy = vacancyRepository.findAll();

        //then
        Assertions.assertEquals(0, foundVacancy.size());

        Assertions.assertTrue(foundVacancy.isEmpty());
    }

    @Test
    public void findAll_happyPath(){
        //given
        addVacancyToDataBase();

        //when
        List<VacancyEntity> foundVacancy = vacancyRepository.findAll();

        //then
        Assertions.assertEquals(1, foundVacancy.size());
    }

    @Test
    public void upDate_happyPath() {
        //given
        VacancyEntity existingVacancy = addVacancyToDataBase();
        existingVacancy.setName("update");
        existingVacancy.setPosition("update");
        existingVacancy.setSalary(10);
        existingVacancy.setCompanyName("update");

        //when
        VacancyEntity upDatedUser = vacancyRepository.upDate(existingVacancy);

        //then
        Assertions.assertEquals(existingVacancy.getVacancyId(), upDatedUser.getVacancyId());
        vacancyRepository.findById(existingVacancy.getVacancyId());

        Assertions.assertEquals(existingVacancy.getName(), upDatedUser.getName());
        Assertions.assertEquals(existingVacancy.getPosition(), upDatedUser.getPosition());
        Assertions.assertEquals(existingVacancy.getSalary(), upDatedUser.getSalary());
        Assertions.assertEquals(existingVacancy.getCompanyName(), upDatedUser.getCompanyName());
    }

    @Test
    public void delete_HappyPath(){
        //given
        VacancyEntity existVacancy = addVacancyToDataBase();
        //when
        vacancyRepository.delete(existVacancy.getVacancyId());
        //then
        VacancyEntity foundVacancy = vacancyRepository.findById(existVacancy.getVacancyId());
        Assertions.assertNull(foundVacancy);
    }

    private VacancyEntity addVacancyToDataBase() {
        VacancyEntity vacancyToAdd = GenerateUtil.prepareVacancy();
        return vacancyRepository.create(vacancyToAdd);
    }


}
