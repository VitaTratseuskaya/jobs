package by.itstep.jobs.mapper;

import by.itstep.jobs.dto.InteviewDto.InterviewCreateDto;
import by.itstep.jobs.dto.InteviewDto.InterviewFullDto;
import by.itstep.jobs.dto.InteviewDto.InterviewShortDto;
import by.itstep.jobs.dto.UserDto.UserCreateDto;
import by.itstep.jobs.dto.UserDto.UserFullDto;
import by.itstep.jobs.dto.UserDto.UserShortDto;
import by.itstep.jobs.dto.VacancyDto.VacancyShortDto;
import by.itstep.jobs.entity.InterviewEntity;
import by.itstep.jobs.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class InterviewMapper {

    private UserMapper userMapper;
    private VacancyMapper vacancyMapper;

    @Autowired
    public InterviewMapper(@Lazy UserMapper userMapper, @Lazy VacancyMapper vacancyMapper) {
        this.userMapper = userMapper;
        this.vacancyMapper = vacancyMapper;
    }

    public InterviewFullDto map(InterviewEntity entity) {
        InterviewFullDto dto = new InterviewFullDto();
        dto.setInterviewId(entity.getInterviewId());
        dto.setStatus(entity.getStatus());
        dto.setInterviewDate(entity.getInterviewDate());

        UserShortDto userDto = userMapper.mapToShort(entity.getUser());
        dto.setUser(userDto);

        VacancyShortDto vacancyDto = vacancyMapper.mapToShort(entity.getVacancy());
        dto.setVacancy(vacancyDto);
        return dto;
    }

    public List<InterviewShortDto> map(List<InterviewEntity> entities) {
        List<InterviewShortDto> dtos = new ArrayList<>();
        for (InterviewEntity entity : entities) {
            InterviewShortDto dto = new InterviewShortDto();
            dto.setInterviewId(entity.getInterviewId());
            dto.setStatus(entity.getStatus());
            dto.setInterviewDate(entity.getInterviewDate());

            dtos.add(dto);
        }
        return dtos;
    }

    public InterviewEntity map(InterviewCreateDto dto) {
        InterviewEntity entity = new InterviewEntity();
        entity.setStatus(dto.getStatus());

        return entity;
    }

    public InterviewShortDto mapToShort(InterviewEntity entity) {
        InterviewShortDto dto = new InterviewShortDto();
        dto.setInterviewId(entity.getInterviewId());
        dto.setStatus(entity.getStatus());
        dto.setInterviewDate(entity.getInterviewDate());

        UserShortDto userDto = userMapper.mapToShort(entity.getUser());
        dto.setUserId(userDto.getUserId());

        VacancyShortDto vacancyDto = vacancyMapper.mapToShort(entity.getVacancy());
        dto.setVacancyId(vacancyDto.getVacancyId());
        return dto;
    }


}
