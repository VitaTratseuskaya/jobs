package by.itstep.jobs.repository;

import by.itstep.jobs.entity.UserEntity;
import by.itstep.jobs.repository.interfaces.UserRepository;
import by.itstep.jobs.util.GenerateUtil;
import by.itstep.jobs.utils.DataBaseCleaner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private DataBaseCleaner cleaner;

    @BeforeEach
    private void setUp() {
        cleaner.clean();
    }

    @AfterEach
    private void shutDown() {
        cleaner.clean();
    }

    @Test
    public void create_happyPath(){
        //given
        UserEntity user = GenerateUtil.prepareUser();

        //when
        UserEntity createdUser = userRepository.create(user);

        //then
        Assertions.assertNotNull(createdUser.getUserId());
        UserEntity foundUser = userRepository.findById(createdUser.getUserId());

        Assertions.assertEquals(user.getRole(), foundUser.getRole());
        Assertions.assertEquals(user.getFirstName(), foundUser.getFirstName());
        Assertions.assertEquals(user.getPhone(), foundUser.getPhone());
        Assertions.assertEquals(user.getEmail(), foundUser.getEmail());
        Assertions.assertEquals(user.getPassword(), foundUser.getPassword());
        Assertions.assertEquals(user.getPosition(), foundUser.getPosition());
        Assertions.assertEquals(user.getYearOfExperience(), foundUser.getYearOfExperience());


    }

    @Test
    public void findById_happyPath() {
        //given
        UserEntity user = addUserToDataBase();
        //when
        UserEntity found = userRepository.findById(user.getUserId());
        //then
        Assertions.assertEquals(user.getUserId(), found.getUserId());
    }

    @Test
    public void findAll_whenNoOneFound(){
        //given

        //when
        List<UserEntity> foundUsers = userRepository.findAll();

        //then
        Assertions.assertEquals(0, foundUsers.size());

        Assertions.assertTrue(foundUsers.isEmpty());
    }

    @Test
    public void findAll_happyPath(){
        //given
        addUserToDataBase();

        //when
        List<UserEntity> foundUsers = userRepository.findAll();

        //then
        Assertions.assertEquals(1, foundUsers.size());
    }

    @Test
    public void upDate_happyPath() {
        //given
        UserEntity existingUser = addUserToDataBase();
        existingUser.setRole("update");
        existingUser.setFirstName("update");
        existingUser.setLastName("update");
        existingUser.setPhone("update");
        existingUser.setEmail("update");
        existingUser.setPassword("update");
        existingUser.setPosition("update");
        existingUser.setYearOfExperience(1);

        //when
        UserEntity upDatedUser = userRepository.upDate(existingUser);

        //then
        Assertions.assertEquals(existingUser.getUserId(), upDatedUser.getUserId());
        userRepository.findById(existingUser.getUserId());

        Assertions.assertEquals(existingUser.getRole(), upDatedUser.getRole());
        Assertions.assertEquals(existingUser.getFirstName(), upDatedUser.getFirstName());
        Assertions.assertEquals(existingUser.getLastName(), upDatedUser.getLastName());
        Assertions.assertEquals(existingUser.getPhone(), upDatedUser.getPhone());
        Assertions.assertEquals(existingUser.getEmail(), upDatedUser.getEmail());
        Assertions.assertEquals(existingUser.getPassword(), upDatedUser.getPassword());
        Assertions.assertEquals(existingUser.getPosition(), upDatedUser.getPosition());
        Assertions.assertEquals(existingUser.getYearOfExperience(), upDatedUser.getYearOfExperience());
    }

    @Test
    public void delete_HappyPath(){
        //given
        UserEntity existUser = addUserToDataBase();
        //when
        userRepository.delete(existUser.getUserId());
        //then
        UserEntity foundUser = userRepository.findById(existUser.getUserId());
        Assertions.assertNull(foundUser);
    }


    private UserEntity addUserToDataBase() {
        UserEntity userToAdd = GenerateUtil.prepareUser();
        return userRepository.create(userToAdd);
    }
}
