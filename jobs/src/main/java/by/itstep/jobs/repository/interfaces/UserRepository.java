package by.itstep.jobs.repository.interfaces;

import by.itstep.jobs.entity.UserEntity;
import by.itstep.jobs.entity.VacancyEntity;

import java.util.List;

public interface UserRepository {

    UserEntity findById(int id);

    List< UserEntity> findAll();

    UserEntity create(UserEntity entity);

    UserEntity upDate(UserEntity entity);

    void delete(int id);

    void deleteAll();

    UserEntity findByEmail(String userEmail);
}
