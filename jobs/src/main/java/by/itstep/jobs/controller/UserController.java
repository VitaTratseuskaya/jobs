package by.itstep.jobs.controller;

import by.itstep.jobs.dto.UserDto.UserCreateDto;
import by.itstep.jobs.dto.UserDto.UserFullDto;
import by.itstep.jobs.dto.UserDto.UserShortDto;
import by.itstep.jobs.dto.UserDto.UserUpdateDto;
import by.itstep.jobs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public UserShortDto create(@Valid @RequestBody UserCreateDto createRequest) {
        UserShortDto createdUser = userService.create(createRequest);
        return createdUser;
    }

    @ResponseBody
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public UserFullDto findById(@PathVariable int id) {
        UserFullDto user = userService.findById(id);
        return user;
    }

    @ResponseBody
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<UserShortDto> findAll() {
        List<UserShortDto> allUsers = userService.findAll();
        return allUsers;
    }

    @ResponseBody
    @RequestMapping(value = "/users", method = RequestMethod.PUT)
    public UserShortDto upDate(@Valid @RequestBody UserUpdateDto upDateRequest) {
        UserShortDto upDateUser = userService.upDate(upDateRequest);
        return upDateUser;
    }

    @ResponseBody
    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int id) {
        userService.delete(id);
    }
}









