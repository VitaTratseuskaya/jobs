package by.itstep.jobs.dto.InteviewDto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
public class InterviewUpdateDto {

    @NotNull
    private Integer interviewId;

    @NotBlank(message = "Name can not by blank")
    private String status;

    @NotNull
    private Date interviewDate;

}
