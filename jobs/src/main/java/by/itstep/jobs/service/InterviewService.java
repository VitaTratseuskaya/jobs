package by.itstep.jobs.service;

import by.itstep.jobs.dto.InteviewDto.InterviewCreateDto;
import by.itstep.jobs.dto.InteviewDto.InterviewFullDto;
import by.itstep.jobs.dto.InteviewDto.InterviewShortDto;
import by.itstep.jobs.dto.InteviewDto.InterviewUpdateDto;
import by.itstep.jobs.entity.InterviewEntity;
import by.itstep.jobs.entity.UserEntity;
import by.itstep.jobs.entity.VacancyEntity;
import by.itstep.jobs.mapper.InterviewMapStruct;
import by.itstep.jobs.mapper.InterviewMapper;
import by.itstep.jobs.repository.InterviewHibernateRepository;
import by.itstep.jobs.repository.UserHibernateRepository;
import by.itstep.jobs.repository.VacancyHibernateRepository;
import by.itstep.jobs.repository.interfaces.InterviewRepository;
import by.itstep.jobs.repository.interfaces.UserRepository;
import by.itstep.jobs.repository.interfaces.VacancyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InterviewService {

    @Autowired
    private InterviewRepository interviewRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private VacancyRepository vacancyRepository;


    public InterviewFullDto findById(int id) {
        InterviewEntity interview = interviewRepository.findById(id);
        if (interview == null) {
            throw new RuntimeException("Interview not found by id " + id);
        }

        System.out.println("InterviewService -> Found interview: " + interview);

        return InterviewMapStruct.MAPPER.mapToFullDto(interview);
    }

    public List<InterviewShortDto> findAll() {
        List<InterviewEntity> allInterview = interviewRepository.findAll();

        if (allInterview.isEmpty()) {
            System.out.println("List is empty!");
        }

        return InterviewMapStruct.MAPPER.mapToListShortDto(allInterview);
    }

    public InterviewFullDto upDate(InterviewUpdateDto updateRequest) {
        InterviewEntity interview = interviewRepository.findById(updateRequest.getInterviewId());

        if (interview == null) {
            throw new RuntimeException("Interview is not found by id " + interview.getInterviewId());
        }

        if (updateRequest.getInterviewId() == null) {
            throw new RuntimeException("InterviewService -> " +
                    "Can't update entity without id " + updateRequest.getInterviewId());
        }

        UserEntity user = userRepository.findById(interview.getUser().getUserId());
        if (user == null) {
            System.out.println("InterviewService -> not found user: " + interview.getUser().getUserId());
        }

        VacancyEntity vacancy = vacancyRepository.findById(interview.getVacancy().getVacancyId());
        if (vacancy == null) {
            System.out.println("InterviewService -> not found vacancy: " + interview.getVacancy().getVacancyId());
        }

        if (interviewRepository.findById(interview.getInterviewId()) == null) {
            throw new RuntimeException("InterviewService -> interview not found by id: " + interview.getInterviewId());
        }

        interview.setUser(userRepository.findById(interview.getUser().getUserId()));
        interview.setVacancy(vacancyRepository.findById(interview.getVacancy().getVacancyId()));

        interview.setStatus(updateRequest.getStatus());
        interview.setInterviewDate(updateRequest.getInterviewDate());

        InterviewEntity interviewUpdate = interviewRepository.upDate(interview);
        System.out.println("InterviewService -> Found interview: " + interviewUpdate);
        return InterviewMapStruct.MAPPER.mapToFullDto(interviewUpdate);
    }

    public InterviewShortDto create(InterviewCreateDto createdRequest) {
        InterviewEntity interview = InterviewMapStruct.MAPPER.mapToEntity(createdRequest);

        if (interview.getInterviewId() != null) {
            throw new RuntimeException("InterviewService -> " +
                    "Can't update entity without id ");
        }

        UserEntity user = userRepository.findById(createdRequest.getUserId());
        interview.setUser(user);
        if (user == null) {
            throw new RuntimeException("InterviewService -> User not found by id " + createdRequest.getUserId());
        }

        VacancyEntity vacancy = vacancyRepository.findById(createdRequest.getVacancyId());
        interview.setVacancy(vacancy);
        if (vacancy == null) {
            throw new RuntimeException("InterviewService -> Vacancy not found by id " + createdRequest.getVacancyId());
        }

        interview.setUser(userRepository.findById(createdRequest.getUserId()));
        interview.setVacancy(vacancyRepository.findById(createdRequest.getVacancyId()));

        InterviewEntity createdInterview = interviewRepository.create(interview);
        System.out.println("InterviewService -> Created interview: " + interview);
        return InterviewMapStruct.MAPPER.mapToShort(createdInterview);
    }

    public void delete(int id) {
        InterviewEntity interviewForDelete = interviewRepository.findById(id);
        if (interviewForDelete == null) {
            throw new RuntimeException("InterviewService -> Interview not found " + interviewForDelete.getInterviewId());
        }

        List<InterviewEntity> allInterview = interviewRepository.findAll();
        for (InterviewEntity interview : allInterview) {
            interviewRepository.delete(interview.getInterviewId());
        }
    }

}
