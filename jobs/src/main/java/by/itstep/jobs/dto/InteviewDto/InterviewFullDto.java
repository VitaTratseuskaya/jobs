package by.itstep.jobs.dto.InteviewDto;


import by.itstep.jobs.dto.UserDto.UserShortDto;
import by.itstep.jobs.dto.VacancyDto.VacancyShortDto;
import lombok.Data;

import java.sql.Date;

@Data
public class InterviewFullDto {

    private Integer interviewId;

    private String status;
    private Date interviewDate;
    private UserShortDto user;
    private VacancyShortDto vacancy;

}
