package by.itstep.jobs.mapper;

import by.itstep.jobs.dto.VacancyDto.VacancyCreateDto;
import by.itstep.jobs.dto.VacancyDto.VacancyFullDto;
import by.itstep.jobs.dto.VacancyDto.VacancyShortDto;
import by.itstep.jobs.dto.VacancyDto.VacancyUpdateDto;
import by.itstep.jobs.entity.VacancyEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface VacancyMapStruct {


    VacancyMapStruct MAPPER = Mappers.getMapper(VacancyMapStruct.class);

    VacancyFullDto mapToFullDto(VacancyEntity entity);

    List<VacancyShortDto> mapToListVacancyShortDto(List<VacancyEntity> entities);

    VacancyEntity mapToEntity(VacancyCreateDto dto);

    VacancyEntity mapToEntity(VacancyUpdateDto dto);

    VacancyShortDto mapToShort(VacancyEntity entity);
}
