package by.itstep.jobs.repository;

import by.itstep.jobs.entity.InterviewEntity;
import by.itstep.jobs.entity.UserEntity;
import by.itstep.jobs.entity.VacancyEntity;
import by.itstep.jobs.repository.interfaces.InterviewRepository;
import by.itstep.jobs.repository.interfaces.UserRepository;
import by.itstep.jobs.repository.interfaces.VacancyRepository;
import by.itstep.jobs.util.GenerateUtil;
import by.itstep.jobs.utils.DataBaseCleaner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class InterviewRepositoryTest {

    @Autowired
    private InterviewRepository interviewRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private VacancyRepository vacancyRepository;

    private DataBaseCleaner cleaner;

    @BeforeEach
    private void setUp() {
        cleaner.clean();
    }

    @AfterEach
    private void shutDown() {
        cleaner.clean();
    }

    @Test
    public void create_happyPath() {
        //given
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        VacancyEntity vacancy = GenerateUtil.prepareVacancy();
        vacancyRepository.create(vacancy);

        InterviewEntity interview = GenerateUtil.prepareInterview(user, vacancy);

        //when
        InterviewEntity createdInterview = interviewRepository.create(interview);

        //then
        Assertions.assertNotNull(createdInterview.getInterviewId());
        InterviewEntity foundInterview = interviewRepository.findById(createdInterview.getInterviewId());

        Assertions.assertEquals(interview.getStatus(), foundInterview.getStatus());
        Assertions.assertEquals(interview.getInterviewDate(), interview.getInterviewDate());
        Assertions.assertEquals(interview.getUser().getUserId(), foundInterview.getUser().getUserId());
        Assertions.assertEquals(interview.getVacancy().getVacancyId(), foundInterview.getVacancy().getVacancyId());
    }

    @Test
    public void findById_happyPath() {
        //given
        InterviewEntity interview = addInterviewToDataBase();
        //when
        InterviewEntity found = interviewRepository.findById(interview.getInterviewId());
        //then
        Assertions.assertEquals(interview.getInterviewId(), found.getInterviewId());
    }

    @Test
    public void findAll_whenNoOneFound() {
        //given

        //when
        List<InterviewEntity> foundInterview = interviewRepository.findAll();

        //then
        Assertions.assertEquals(0, foundInterview.size());

        Assertions.assertTrue(foundInterview.isEmpty());
    }

    @Test
    public void findAll_happyPath() {
        //given
        addInterviewToDataBase();

        //when
        List<InterviewEntity> foundInterview = interviewRepository.findAll();

        //then
        Assertions.assertEquals(1, foundInterview.size());
    }

    @Test
    public void upDate_happyPath() {
        //given
        InterviewEntity existingInterview = addInterviewToDataBase();

        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        VacancyEntity vacancy = GenerateUtil.prepareVacancy();
        vacancyRepository.create(vacancy);

        existingInterview.setUser(user);
        existingInterview.setVacancy(vacancy);
        existingInterview.setStatus("update");
        existingInterview.setInterviewDate(existingInterview.getInterviewDate());

        //when
        InterviewEntity upDatedInterview = interviewRepository.upDate(existingInterview);

        //then
        Assertions.assertEquals(existingInterview.getInterviewId(), upDatedInterview.getInterviewId());
        interviewRepository.findById(existingInterview.getInterviewId());

        Assertions.assertEquals(existingInterview.getUser(), upDatedInterview.getUser());
        Assertions.assertEquals(existingInterview.getVacancy(), upDatedInterview.getVacancy());
        Assertions.assertEquals(existingInterview.getStatus(), upDatedInterview.getStatus());
        Assertions.assertEquals(existingInterview.getInterviewDate(), upDatedInterview.getInterviewDate());

    }

    @Test
    public void delete_HappyPath() {
        //given
        InterviewEntity existInterview = addInterviewToDataBase();
        //when
        interviewRepository.delete(existInterview.getInterviewId());
        //then
        InterviewEntity foundInterview = interviewRepository.findById(existInterview.getInterviewId());
        Assertions.assertNull(foundInterview);
    }

    private InterviewEntity addInterviewToDataBase() {
        UserEntity user = GenerateUtil.prepareUser();
        userRepository.create(user);

        VacancyEntity vacancy = GenerateUtil.prepareVacancy();
        vacancyRepository.create(vacancy);

        InterviewEntity interviewToAdd = GenerateUtil.prepareInterview(user, vacancy);
        return interviewRepository.create(interviewToAdd);
    }

}
