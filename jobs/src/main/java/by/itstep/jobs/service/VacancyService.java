package by.itstep.jobs.service;

import by.itstep.jobs.dto.VacancyDto.VacancyCreateDto;
import by.itstep.jobs.dto.VacancyDto.VacancyFullDto;
import by.itstep.jobs.dto.VacancyDto.VacancyShortDto;
import by.itstep.jobs.dto.VacancyDto.VacancyUpdateDto;
import by.itstep.jobs.entity.VacancyEntity;
import by.itstep.jobs.mapper.VacancyMapStruct;
import by.itstep.jobs.mapper.VacancyMapper;
import by.itstep.jobs.repository.interfaces.VacancyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VacancyService {

    @Autowired
    private VacancyRepository vacancyRepository;


    public VacancyShortDto create(VacancyCreateDto createdRequest) {
        VacancyEntity vacancyForCreate = VacancyMapStruct.MAPPER.mapToEntity(createdRequest);

        if (vacancyForCreate.getVacancyId() != null) {
            throw new RuntimeException("VacancyService -> Can't create entity which already has id");
        }

        VacancyEntity createdVacancy = vacancyRepository.create(vacancyForCreate);
        System.out.println("VacancyService -> Created vacancy: " + createdRequest);
        return VacancyMapStruct.MAPPER.mapToShort(createdVacancy);
    }

    public VacancyShortDto findById(int id) {
        VacancyEntity vacancy = vacancyRepository.findById(id);

        if (vacancy == null) {
            throw new RuntimeException("Vacancy not found by id " + id);
        }
        System.out.println("VacancyService -> Found vacancy: " + vacancy);
        return VacancyMapStruct.MAPPER.mapToShort(vacancy);
    }

    public List<VacancyShortDto> findAll() {
        List<VacancyEntity> allVacancy = vacancyRepository.findAll();
        System.out.println("VacancyService -> Found vacancy: " + allVacancy);

        if (allVacancy.isEmpty()) {
            System.out.println("List vacancy is empty!");
        }
        return VacancyMapStruct.MAPPER.mapToListVacancyShortDto(allVacancy);
    }

    public VacancyShortDto upDate(VacancyUpdateDto updateRequest) {
        VacancyEntity vacancyForUpdate = VacancyMapStruct.MAPPER.mapToEntity(updateRequest);
                vacancyRepository.findById(updateRequest.getVacancyId());
        if(vacancyForUpdate == null){
            throw new RuntimeException("User is not found by id " + vacancyForUpdate.getVacancyId());

        }

        if (updateRequest.getVacancyId() == null) {
            throw new RuntimeException("VacancyService -> " +
                    "Can't update entity without id " + updateRequest.getVacancyId());
        }

        if (vacancyRepository.findById(updateRequest.getVacancyId()) == null) {
            throw new RuntimeException("CollectionService -> Collection not found by id: " + updateRequest.getVacancyId());
        }

        vacancyForUpdate.setName(updateRequest.getName());
        vacancyForUpdate.setSalary(updateRequest.getSalary());

        VacancyEntity vacancyUpdate = vacancyRepository.upDate(vacancyForUpdate);
        System.out.println("VacancyService -> Update vacancy: " + vacancyForUpdate.getVacancyId());

        return VacancyMapStruct.MAPPER.mapToShort(vacancyUpdate);
    }

    public void delete(int id) {
        VacancyEntity vacancyForDelete = vacancyRepository.findById(id);
        if (vacancyForDelete == null) {
            throw new RuntimeException("VacancyService -> Vacancy not found " + vacancyForDelete.getVacancyId());
        }
        vacancyRepository.delete(id);
    }
}
