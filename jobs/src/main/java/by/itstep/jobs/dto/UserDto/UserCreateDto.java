package by.itstep.jobs.dto.UserDto;


import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserCreateDto {

    @NotNull(message = "Email must have invalid format")
    @Email(message = "Email can not be null")
    private String email;

    @NotBlank(message = "First name can not by blank")
    private String firstName;

    @NotBlank(message = "Last name can not by blank")
    private String lastName;

    @NotBlank(message = "Phone can not by blank")
    @Size(min = 9, max = 9, message = "The number must have 9 digits")
    private String phone;

    @NotBlank(message = "Role can not by blank")
    private String role;

    @NotBlank(message = "Password can not by blank")
    @Size(min = 8, max = 100, message = "Password length must between 8 and 100")
    private String password;

    @NotBlank(message = "Position can not by blank")
    private String position;

    private Integer yearOfExperience;

}
