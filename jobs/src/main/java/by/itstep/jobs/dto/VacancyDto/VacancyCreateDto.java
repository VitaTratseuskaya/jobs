package by.itstep.jobs.dto.VacancyDto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class VacancyCreateDto {

    @NotBlank(message = "Name can not by blank")
    private String name;

    @NotBlank(message = "Position can not by blank")
    private String position;


    private Integer salary;

    @NotBlank(message = "Company name can not by blank")
    private String companyName;



}
