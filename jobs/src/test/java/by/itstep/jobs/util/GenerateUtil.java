package by.itstep.jobs.util;

import by.itstep.jobs.dto.InteviewDto.InterviewCreateDto;
import by.itstep.jobs.dto.InteviewDto.InterviewUpdateDto;
import by.itstep.jobs.dto.UserDto.UserCreateDto;
import by.itstep.jobs.dto.UserDto.UserUpdateDto;
import by.itstep.jobs.dto.VacancyDto.VacancyCreateDto;
import by.itstep.jobs.dto.VacancyDto.VacancyUpdateDto;
import by.itstep.jobs.entity.InterviewEntity;
import by.itstep.jobs.entity.UserEntity;
import by.itstep.jobs.entity.VacancyEntity;
import by.itstep.jobs.repository.interfaces.InterviewRepository;
import by.itstep.jobs.repository.interfaces.UserRepository;
import by.itstep.jobs.repository.interfaces.VacancyRepository;
import com.github.javafaker.Faker;

import java.sql.Date;
import java.util.Random;

public class GenerateUtil {

    private static UserRepository userRepository;
    private static VacancyRepository vacancyRepository;
    private static InterviewRepository interviewRepository;

    public static Faker faker = new Faker(new Random());

    public static VacancyEntity prepareVacancy(){
        VacancyEntity vacancy = new VacancyEntity();
        vacancy.setName(faker.name().username());
        vacancy.setPosition(faker.ancient().god());
        vacancy.setSalary(faker.number().randomDigitNotZero());
        vacancy.setCompanyName(faker.company().name());

        return vacancy;
    }

    public static VacancyCreateDto prepareVacancyCreateDto() {
        VacancyCreateDto vacancy = new VacancyCreateDto();
        vacancy.setName(faker.name().fullName());
        vacancy.setPosition(faker.harryPotter().book());
        vacancy.setSalary(faker.number().randomDigit());
        vacancy.setCompanyName(faker.company().industry());

        return vacancy;
    }

    public static VacancyUpdateDto prepareVacancyUpdateDto() {
        VacancyUpdateDto vacacny = new VacancyUpdateDto();
        vacacny.setName(faker.name().fullName());
        vacacny.setSalary(faker.number().randomDigitNotZero());

        return vacacny;
    }

    public static UserEntity prepareUser(){
        UserEntity user = new UserEntity();

        user.setRole(faker.weather().description());
        user.setFirstName(faker.name().firstName());
        user.setLastName(faker.name().lastName());
        user.setPhone(faker.phoneNumber().phoneNumber());
        user.setEmail(faker.name().name());
        user.setPassword(faker.number().digit());
        user.setPosition(faker.pokemon().name());
        user.setYearOfExperience(faker.number().numberBetween(0, 50));

        return user;
    }

    public static UserCreateDto prepareUserCreateDto(){
        UserCreateDto user = new UserCreateDto();
        user.setEmail(faker.harryPotter().location());
        user.setPassword(faker.number().digit());
        user.setPosition(faker.pokemon().location());
        user.setPhone(faker.phoneNumber().phoneNumber());
        user.setRole(faker.weather().description());
        user.setFirstName(faker.name().firstName());
        user.setYearOfExperience(faker.number().randomDigitNotZero());
        user.setLastName(faker.name().lastName());

        return user;
    }

    public static UserUpdateDto prepareUserUpdateDto(){
        UserUpdateDto user = new UserUpdateDto();
        user.setFirstName(faker.name().firstName());
        user.setPhone(faker.phoneNumber().phoneNumber());
        user.setLastName(faker.slackEmoji().people());
        user.setPosition(faker.pokemon().name());
        user.setYearOfExperience(faker.number().randomDigitNotZero());
        return user;
    }

    public static InterviewEntity prepareInterview(UserEntity user, VacancyEntity vacancy) {
        InterviewEntity interview = new InterviewEntity();

        interview.setStatus(faker.ancient().god());
        interview.setInterviewDate(new Date(faker.date().birthday().getDate()));
        interview.setUser(user);
        interview.setVacancy(vacancy);

        return interview;
    }

    public static InterviewCreateDto prepareInterviewCreateDto(UserEntity user, VacancyEntity vacancy){
        InterviewCreateDto interview = new InterviewCreateDto();
        interview.setStatus(faker.beer().name());
        interview.setUserId(user.getUserId());
        interview.setVacancyId(vacancy.getVacancyId());

        return  interview;
    }

    public static InterviewUpdateDto prepareInterviewUpdateDto(UserEntity user, VacancyEntity vacancy) {
        InterviewUpdateDto interview = new InterviewUpdateDto();
        interview.setInterviewDate(interview.getInterviewDate());
        interview.setStatus(faker.ancient().primordial());

        return interview;
    }
}
