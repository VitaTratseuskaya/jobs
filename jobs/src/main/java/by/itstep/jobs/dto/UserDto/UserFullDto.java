package by.itstep.jobs.dto.UserDto;


import by.itstep.jobs.dto.InteviewDto.InterviewShortDto;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserFullDto {

    private Integer userId;
    private String role;
    private String firstName;
    private String lastName;

    private String phone;
    private String email;
    private String position;
    private Integer yearOfExperience;

    private List<InterviewShortDto> interviews = new ArrayList<>();
}
